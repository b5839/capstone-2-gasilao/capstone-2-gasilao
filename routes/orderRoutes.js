const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");

// Route for (Create Order) Non-Admin User Checkout

router.post("/checkout", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		res.send("You are not allowed to checkout order!");
	}
	else{
		orderControllers.checkOut(userData.id).then(orderController => res.send (orderController));
	}

})

// Route for retrieving All Orders

router.get("/", auth.verify, (req, res) =>{

	const getAllOrder = auth.decode(req.headers.authorization);
	if(getAllOrder.isAdmin){
		orderControllers.getAllOrders().then(resultFromController => res.send(resultFromController));

	}
	else{
		res.send("You are not an admin to get all order");
	}
})

// Route for authenticated User's Order

router.get("/myOrders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);

	
	if(userData.isAdmin){
		res.send("You are not allowed to get user's order");
	}
	else{
		orderControllers.getMyOrders(userData.id).then(orderController => res.send (orderController));
	}


})

router.post("/addProduct", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	console.log(req.body);
	if(userData.isAdmin){
		res.send("You are not allowed to add product");
	}
	else{
		orderControllers.addMyproducts(req.body).then(orderController => res.send (orderController));
	}
})

module.exports = router;

