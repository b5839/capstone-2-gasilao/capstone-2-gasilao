const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth");

// Route for creating a product

router.post("/", auth.verify, (req, res) =>{
	const createProduct = auth.decode(req.headers.authorization);
	console.log(createProduct.isAdmin);

	if(createProduct.isAdmin){
		productControllers.addProduct(req.body).then(resultFromControllers => res.send(resultFromControllers));
	}
	else{
		res.send("You are not an admin to create product!");
	}
})

// Route for retrieving all active products

router.get("/",(req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a single product

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Route to Update product

router.put("/:productId", auth.verify, (req, res) => {
	const updateProduct = auth.decode(req.headers.authorization);
	console.log(updateProduct.isAdmin);

	if(updateProduct.isAdmin){
		productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission to update this product!")
	}
})

// Route to Archive a product

router.patch("/:productId/archive", auth.verify, (req, res) => {
	const archiveCourse = auth.decode(req.headers.authorization);
	console.log(archiveCourse.isAdmin);

	if(archiveCourse.isAdmin){
		productControllers.archiveProduct(req.params.productId,).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send("You don't have permission to archive product!");
	}
	
})

module.exports = router;