const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// Router for the user registration

router.post("/register", (req, res) =>{
	userControllers.registerUser(req.body).then(resultFromControllers => res.send(resultFromControllers));
});

// Route for the user login (with token creation)

router.post("/login", (req, res) =>{
	userControllers.loginUser(req.body).then(resultFromControllers => res.send(resultFromControllers));
});

// Route for set up user as admin

router.patch("/:userId/setAsAdmin", (req, res) =>{
	userControllers.setAsAdmin(req.params.userId).then(
		resultFromControllers => res.send(resultFromControllers));
})

module.exports = router;