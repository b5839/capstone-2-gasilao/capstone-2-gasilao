const Order = require("../models/Order");
const Product = require("../models/Product");
// Create Order

module.exports.checkOut=(userId)=>{
let newOrder= new Order({
        userId: userId,
        totalAmount: 0
    })
    return newOrder.save().then((product,error)=>{
        if(error){
            return "checkout Order Unsuccessful";
            //return false;
        }else{
            return "checkout Order successfully Created";
            //return true;
        }
    });
}
module.exports.getAllOrders = () =>{
    return Order.find({}).then(result=>result);
}
//Add Product to Checkout order
module.exports.addMyproducts= async(reqBody)=>{

    let productDetails = await Product.findById(reqBody.productId).then(result=>result);
        
  
      if(productDetails.stocks>=reqBody.quantity){
            let totalAmount = productDetails.price*reqBody.quantity;
              let addOrderProduct = {
                  productId:reqBody.productId,
                  totalAmount:totalAmount,
                  stocks:reqBody.quantity
              }

          let isOrderUpdated=await Order.findById(reqBody.orderId).then(order=>{
            order.product.push(addOrderProduct);
            order.totalAmount=order.totalAmount+totalAmount;
            return order.save().then((enrollment,error)=>{
                if(error){
                    return false;
                }else{ 
                    
                    return true;
                }
            });
        })
     
        let isProductUpdated=await Product.findById(reqBody.productId).then(product=>{
                product.stocks-=reqBody.quantity;
                return product.save().then((enrollee,error)=>{
                    if(error){
            
                        return false;
                    }else{
                        return true;
                    }
                });
        })

        if(isOrderUpdated && isProductUpdated){
                
                return "Successful Adding Product to Order";
                //return true;

        }else{
                return "Adding Product to Order Unsuccessful";
                //return false;

        }
          //return false;
      }else{
          return "not enough Stocks";
          //return false;
     }
     
}

// // Retrieve all orders (Admin only)



// // Retrieve authenticated user's order


module.exports.getMyOrders = (userId) =>{
    return Order.find({userId:userId}).then(result => result);
}




