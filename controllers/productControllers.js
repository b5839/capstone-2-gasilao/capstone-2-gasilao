const Product = require("../models/Product");

// Create a product
module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})

	return newProduct.save().then((course, error) =>{
		if(error){
			return false
		}
		else{
			return "You have successfully created a product";
		}
	})
}

// Retrive all Active Products

module.exports.getAllActive = () =>{
	return Product.find({isActive: true}).then(result => result);
}

// Retrieve a single Product

module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);
}

// Update a Product

module.exports.updateProduct = (productId, reqBody) =>{
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	return Product.findByIdAndUpdate(productId, updateProduct).then((productUpdate, error) => {
		if(error){
			return false;
		}
		else{
			return "Product successfully updated";
		}
	})
}

// Archive a product

module.exports.archiveProduct = (productId) =>{
	let updateActiveField = {
		isActive: false
	}
	return Product.findByIdAndUpdate(productId, updateActiveField).then((isActive, error) =>{
		if(error){
			return false;
		}
		else{
			return true
		}
	})
}