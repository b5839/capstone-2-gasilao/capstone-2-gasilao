const User = require("../models/User");
// const Product = require ("../models/Product");
// const Order = require ("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})
	
	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			console.log(user);
			return true;
		}
	})
}

// User Authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

		// User does not exist
		if(result == null){
			return false;
		}
		//User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

// set user as admin

module.exports.setAsAdmin = (userId) => {

	return User.findById(userId).then(user =>{
		user.isAdmin = true;
		return user.save().then((userUpdate, error) =>{
			if(error){
				return "User not set as admin.";
			}
			else{
				return "User updated successfully to be admin.";
			}
		})
	})
}