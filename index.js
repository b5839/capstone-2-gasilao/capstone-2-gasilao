// Require Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

// server
const app = express();
const port = 4000;

// connection to MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.ni5hwok.mongodb.net/C2_E-commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true 
});

//Set notification for connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open",() => console.log("We're connected to the cloud database."))

// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

//Routes for our API
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Listening to port
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`);
})