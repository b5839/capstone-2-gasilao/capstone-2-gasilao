const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	
	totalAmount:{
		type: Number,
		required: [true, "Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId:{
		type: String,
		required: [true, "UserId is Required"]
	},
	product:[
	{
		productId: {
			type: String,
			required: [true, "productId is Required"]
			}
		}	
	]

})
module.exports = mongoose.model("Order", orderSchema);